import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by vlad on 10/20/2016.
 */

public class Main {
///https://github.com/davinkevin/Podcast-Server/blob/master/Backend/src/main/java/lan/dk/podcastserver/service/FfmpegService.java



    public static void main(String[] args) {

        FFmpeg ffmpeg = null;
        try {
            ffmpeg = new FFmpeg("D:\\dev\\ffmpeg\\bin\\ffmpeg.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path out = Paths.get("D:\\dev\\output_x264E_xvidE_mpeg4E.mp4");

        Path in = Paths.get("D:\\dev\\mb.mp4");
        Path in2 = Paths.get("D:\\dev\\mb1.mp4");
        FFprobe ffprobe = new FFprobe("D:\\dev\\ffmpeg\\bin\\ffprobe.exe");
        FFmpegProbeResult ins = null;
        try {
            ins = ffprobe.probe("D:\\video\\h.264\\mpeg4\\PPAP_720.mp4");
        } catch (IOException e) {
            e.printStackTrace();
        }

        FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);

        MergeVideo.concat(out,executor,in,in2);
        //CutVideo.cutVideo(ins,executor,500,3500);
        //encoding(ins,executor);
        //MediaInfo.getMediaInfo(ins);
    }

    public static void encoding (FFmpegProbeResult in, FFmpegExecutor executor) {

        FFmpegBuilder builder = new FFmpegBuilder()

                .setInput(in) // Filename, or a FFmpegProbeResult
                .overrideOutputFiles(true) // Override the output if it exists

                .addOutput("D:\\video\\h.264\\mpeg4\\PPAP_720E.mp4")   // Filename for the destination
                .setFormat("mp4")        // Format is inferred from filename, or can be set
                .setVideoResolution(1280,720) // at 640x480 resolution
                .done();


        // Run a one-pass encode
         executor.createJob(builder).run();
    }

}
