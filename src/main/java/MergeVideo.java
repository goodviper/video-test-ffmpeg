import com.google.common.collect.Lists;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.util.stream.Collectors.joining;

/**
 * Created by user on 10/26/2016.
 */
public class MergeVideo {

    public static final String CODEC_COPY = "copy";
    private static final String FORMAT_CONCAT = "concat";

    /* Concat files */
    public static void concat(Path target, FFmpegExecutor executor, Path... files) {
        Path listOfFiles = null;
        try {
            Files.deleteIfExists(target);

            String filesStrings = Lists.newArrayList(files)
                    .stream()
                    .map(f -> f.getFileName().toString())
                    .map(p -> "file 'D:\\dev\\" + p + "'")
                    .collect(joining(System.getProperty("line.separator")));

            //listOfFiles = Files.createTempFile(target.getParent(), "ffmpeg-list-", ".txt");
            //  Files.write(listOfFiles, filesStrings.getBytes());

            FFmpegBuilder builder = new FFmpegBuilder()
                    //.setInput(listOfFiles.toAbsolutePath().toString())
                    .setInput("D:\\dev\\ffmpeg.txt").addExtraArgs("-safe","0")
                    .setFormat(FORMAT_CONCAT)
                    .addOutput(target.toAbsolutePath().toString())
                    .setAudioCodec(CODEC_COPY)
                    .setVideoCodec(CODEC_COPY)
                    .done();

            executor.createJob(builder).run();

        } catch (IOException e) {

        } finally {
        }
    }
}
